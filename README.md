# individual-project4



## Getting started

### Function
1. In this project, I designed two functions to encrypt and decrypt given sentence using cipher code. They are described in lambda function `encrypt` and `decrypt`.

2. test two functions locally
![](pic/1.png) \
![](pic/2.png)

### AWS Lambda
1. build the functions by running `cargo lambda build --release`
2. deploy the lambda functions by `cargo lambda deploy --iam-role="xxx" --region="xxx"`
![](pic/3.png) \
![](pic/5.png)

3. test the functions online
![](pic/4.png)
![](pic/6.png)

### Step Function
1. create the step function using given lambda function
![](pic/7.png)

2. test the step function and compare the input and output
![](pic/8.png)
