use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

// Define a struct to represent the input data
#[derive(Deserialize, Serialize)]
struct Input {
    data: String, 
}

// Define a struct to represent the output data
#[derive(Deserialize, Serialize)]
struct Output {
    data: String, 
}

// Main function, entry point of the program
#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(process_data);
    lambda_runtime::run(func).await?;
    Ok(())
}

// Function to process the input data
async fn process_data(event: Input, _ctx: Context) -> Result<Output, Error> {
    let decrypted_data = caesar_decipher(&event.data, 3);
    Ok(Output { data: decrypted_data })
}

// Function to decrypt a string using Caesar cipher
fn caesar_decipher(input: &str, shift: usize) -> String {
    input.chars().map(|c| {
        if c.is_ascii_alphabetic() {
            let first = if c.is_ascii_uppercase() { 'A' } else { 'a' } as u8;
            let shift_u8 = shift as u8; // Cast shift to u8
            let shifted = if c as u8 >= first + shift_u8 {
                c as u8 - shift_u8
            } else {
                c as u8 + 26 - shift_u8
            };
            shifted as char
        } else {
            c
        }
    }).collect()
}
