use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

// Define a struct to represent the input data
#[derive(Deserialize, Serialize)]
struct Input {
    data: String, 
}

// Define a struct to represent the output data
#[derive(Deserialize, Serialize)]
struct Output {
    data: String, 
}

// Main function, entry point of the program
#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(process_data);
    lambda_runtime::run(func).await?;
    Ok(())
}

// Function to process the input data
async fn process_data(event: Input, _ctx: Context) -> Result<Output, Error> {
    let encrypted_data = caesar_cipher(&event.data, 3);
    Ok(Output { data: encrypted_data })
}

// Function to encrypt a string using Caesar cipher
fn caesar_cipher(input: &str, shift: usize) -> String {
    input.chars().map(|c| {
        if c.is_ascii_alphabetic() {
            let first = if c.is_ascii_uppercase() { 'A' } else { 'a' } as u8;
            let shifted = (c as u8 - first + shift as u8) % 26 + first;
            shifted as char
        } else {
            c
        }
    }).collect()
}
